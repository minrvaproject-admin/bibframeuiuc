<style type="text/css">
 <!--
#projecttabs-tab-list li a {
	background-color: #dddddd;
	color: #1f376s;
	border-radius: 0px;
	border: 0px;
}

#projecttabs-tab-list li.active a {
	background-color: #1f3762;
	color: #d4e4f3;
	border-radius: 0px;
	border: 0px;
}

.nav>list>a {
	position: relative;
	display: block;
	padding: 10px 15px;
}
 -->
</style>

<!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h2>BIBFRAME at University of Illinois</h2>
        <p>The end result of this project is enhanced discovery of library data – bringing like sets of content together in contemporary and easy to understand views assisting users in locating sets of associated bibliographic metadata.</p>
        <p>"Initiated by the Library of Congress, <a href="http://www.loc.gov/bibframe/">BIBFRAME</a> provides a foundation for the future of bibliographic description, both on the web, and in the broader networked world."</p>
      </div>
    </div>

      <div class="container">

<div style="padding-top: 10px;">
        <p style="font-size:.8em; color: #999;">Last update: June 24, 2017</p>
  </div>
      <div class="tabbable">
  <ul class="nav nav-tabs" id="projecttabs-tab-list">
    <li class="active"><a href="#pane1" data-toggle="tab">About</a></li>
    <li><a href="#pane2" data-toggle="tab">BIBFRAME Search Options</a></li>
    <li><a href="#pane3" data-toggle="tab">Linked Data Sources</a></li>
    <li><a href="#pane4" data-toggle="tab">Publications</a></li>
    <li><a href="#pane5" data-toggle="tab">Code + Acknowledgements</a></li>
  </ul>
  <hr style="margin-top: 0px;">
  <div class="tab-content">
    <div id="pane1" class="tab-pane active">

<!-- TAB PANE CONTENT -->
      <h3>About</h3>
      <div class="row">
  	<div class="col-md-4" style="border-right: 1px solid #CCC;">
    	<h4>Funding + Scope</h4>
        <p></p><p>The project was initiated with funding from an Innovation Grant from the University of Illinois Library to explore transforming MARC records to BIBFRAME linked data. The proposal is available <a href="http://www.library.illinois.edu/committee/exec/innovation_fund/innovation_fund_proposals/2013-2014/preparing_for_the_post-marc_world.html">here.</a></p>
    	<p>The project team chose to focus on the corpus of e-book records in the library catalog. The Illinois Library provides access to nearly 300,000 e-books. <p><p>By the end of the Summer 2015 semester the team completed the e-book work and two sample search interfaces demonstrating the feasibility of transforming & enriching MARC records with linked open data, and demonstrating approaches in retrieval of the records for enhanced discovery. Final Report available at: <a href="http://goo.gl/Azjxkt">http://goo.gl/Azjxkt</a><p>
    </div>
    <div class="col-md-4" style="border-right: 1px solid #CCC;">
    	<h4>Team</h4>
      <p></p><p>The team included librarians Qiang Jin (CAM) and Jim Hahn (UGL), as well as two graduate students: Gretchen Croll (GSLIS), and Suma Vangala (CS).</p>
    </div>

    <div class="col-md-4" style="border-right: 1px solid #CCC;">
    <h4>Additional Collaborators</h4>
      <p></p><p>We collaborated broadly and sought input from multiple metadata experts in the library, including Micheal Norman and Ayla Stein.</p>
      <p></p><p>The BIBFRAME @ Illinois Project is registered with the Library of Congress <a href="http://www.loc.gov/bibframe/implementation/register.html">BIBFRAME implementation Register</a>.
    </div>
  </div><!-- end row -->

<!-- END TAB PANE1 CONTENT -->

  </div>
    <div id="pane2" class="tab-pane">
    <h3>BIBFRAME Search Options</h3>
        <div class="row">
  	<div class="col-md-4" style="border-right: 1px solid #CCC;">
    	<h4>Google Custom Search: Structured Data</h4>
        <p></p><p>This search interface provides results with structured data when <a href="http://sif.library.illinois.edu/bibframe/search.php">retrieving</a> BIBFRAME records.</p>
    </div>
    <div class="col-md-4" style="border-right: 1px solid #CCC;">
    	<h4>E-book Bento view</h4>
      <p></p><p>A pilot implementation of how BIBFRAME records are retrieved in a <a href="http://sif.library.illinois.edu/megasearch/">Bento-style seach.</a></p>
    </div>

    <div class="col-md-4">
    <h4>Sitemaps for BIBFRAME HTML</h4>
      <p></p><p>As a result of the transformation process, there are twenty-nine HTML sitemaps, with 10,000 pages per map available for parsing <a href="http://sif.library.illinois.edu/bibframe/sitemap">here</a>.</p><p>Each HTML file (a BIBFRAME record) encorporates RDF for a BIBFRAME Work, Instance, Authority, and Annotation.<p><p>BIBFRAME HTML also incorporates Schema.org structured data.</p>
  </div>

  </div><!-- end row -->

<!-- END TAB PANE2 CONTENT -->

  </div>
     <div id="pane3" class="tab-pane">
      <h3>Linked Data Sources</h3>

<div class="row">
  	<div class="col-md-3" style="border-right: 1px solid #CCC;">
          <h4>Work Identifier</h4>
          <p></p><p><a href="http://xisbn.worldcat.org/xisbnadmin/index.htm">xISBN: Worldcat Work ID <i class="fa fa-external-link"></i></a></p>

          <h4>Instance Identifier</h4>
          <p></p><p><a href="http://vufind.carli.illinois.edu/vf-uiu/">University of Illinois at Urbana-Champaign Vu-Find Catalog(vu-find) <i class="fa fa-external-link"></i></a></li></p>

          </div>


          <div class="col-md-3" style="border-right: 1px solid #CCC;">
          <h4>Personal Names, Corporate Names, and Geographic Names</h4>
          <p></p><p><a href="http://viaf.org/">The Virtual International Authority File (VIAF) <i class="fa fa-external-link"></i></a></li></p>
          </div>


  	<div class="col-md-3" style="border-right: 1px solid #CCC;">
          <h4>Subject Headings</h4>
          <p></p><p><a href="http://authorities.loc.gov/webvoy.htm">Library of Congress Authority Files (LC/NACO Authority File) <i class="fa fa-external-link"></i></a></li></p>


           <p></p><p><a href="http://id.loc.gov/">LC Linked Data Service: Authorities and Vocabularies <i class="fa fa-external-link"></i></a></li></p>


          <p></p><p><a href="http://experimental.worldcat.org/fast/">Faceted Application of Subject Terminology (FAST) <i class="fa fa-external-link"></i></a></li></p>


           <p></p><p><a href="http://id.nlm.nih.gov/mesh/">Medical Subject Headings (MeSH) RDF Linked Data <i class="fa fa-external-link"></i></a></li></p>

           </div>



       <div class="col-md-3" style="border-right: 1px solid #CCC;">
          <h4>Researchers and Organizations</h4>
          <p></p><p><a href="http://isni.org/">The International Standard Name Identifier (ISNI) <i class="fa fa-external-link"></i></a></li></p><a href="http://orcid.org/">ORCiD <i class="fa fa-external-link"></i></a></li></p>
          </div>

</div><!-- END ROW -->


<!-- END TAB PANE3 CONTENT -->

  </div>
    <div id="pane4" class="tab-pane">
    <h3>Publications</h3>
<div class="row">
    <div class="col-md-3" style="border-right: 1px solid #CCC;">
          <h4>Article: BIBFRAME transformation for enhanced discovery</h4>
          <p></p><p><a href="http://hdl.handle.net/2142/90248">Jin, Q., Hahn, J., Croll, G. (2016). BIBFRAME transformation for enhanced discovery. Library Resources and Technical Services, 60 (4).<i class="fa fa-external-link"></i></a></p>

          <h4>Dataset: BIBFRAME transformation data</h4>
          <p></p><p><a href="https://doi.org/10.13012/B2IDB-1082638_V1 ">Jin, Qiang; Hahn, James; Croll, Gretchen (2016): BIBFRAME Transformation Data. University of Illinois at Urbana-Champaign. https://doi.org/10.13012/B2IDB-1082638_V1<i class="fa fa-external-link"></i></a></li></p>

          </div>
          <div class="col-md-3" style="border-right: 1px solid #CCC;">
          <h4>Presentation: Open Linked Data in Discovery: BIBFRAME + Schema.org in an Experimental Bento View</h4>
          <p></p><p><a href="http://hdl.handle.net/2142/88824">Hahn, J. "Open Linked Data in Discovery: BIBFRAME + Schema.org in an Experimental Bento View," Presentation to MARC Formats Transition Interest Group (ALCTS LITA), American Library Association Midwinter Conference, Boston, MA: January 9, 2016.<i class="fa fa-external-link"></i></a></li></p>
          </div>

           
          <div class="col-md-3" style="border-right: 1px solid #CCC;">
          <h4>Presentation: Tools for Enhanced Discovery on the Web</h4>
          <p></p><p><a href="http://hdl.handle.net/2142/96263">Hahn, J., Jin, Q., "Tools for Enhanced Discovery on the Web," American Library Association Annual Conference, Cataloging and Metadata For the Web: Meeting the User Where They Are Pre-conference, Chicago, IL: Friday, June 23, 2017.<i class="fa fa-external-link"></i></a></li></p>
          </div> 

  </div>
<!-- END TAB PANE4 CONTENT -->
  </div>
    <div id="pane5" class="tab-pane">
    <h3>Code + Acknowledgements</h3>
<p>For BIBBFRAME 1.0 research and development in 2015 we utilized the <a href="https://github.com/lcnetdev/marc2bibframe">Library of Congress XQuery transformation code</a> to begin our process, and then augmented the results by programmatically enriching records with linked open data. Python code developed as a result of the grant is available from <a href="https://bitbucket.org/minrvaproject-admin/bibframeuiuc/overview">Bitbucket.</a></p>
<p></p><p> </p>
  </div>


<!-- closing for container -->


</div>
</div>
</div>
</div>
</div>
