## BIBFRAME @ University of Illinois

### This repository is for the code used to enrich BIBFRAME RDF with Linked Data.###
 
Linked data sources can be viewed from the BIBFRAME @ University of Illinois project page (http://sif.library.illinois.edu/bibframe )

* There are four folders in the project. 

*The main projet code is the transform folder which holds the RDF enrichment code for a BIBFRAME Work, Instance, Annotation, and Authority. 

*SQL Queries folder contains the results of querying our local database for all e-book bibIDs which we then used for MARCXML generation.

*The Getmarxml folder is specific to generating MARCXML at UIUC and utilizes a local microservice for this process. 

*Finally, in order to index the HTML, sitemap.py was developed for generating sitemaps.




### How do I get set up? ###

* Summary of set up

Folder setup only requires an input and output path. The input RDF is generated with the Library of Congress XQuery transformation code marc2bibframe (https://github.com/lcnetdev/marc2bibframe ). 

* Configuration
 Python Requirements are noted in the files. All of the transforms use Python 3.4.0




### Who do I talk to? ###

* Jim Hahn
* jimhahn@illinois.edu