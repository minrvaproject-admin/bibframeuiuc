__author__ = 'jimhahn'

# Code to create sitemaps for all files, uses generated HTML folders as input.

from xml.sax.saxutils import escape
import requests
import sys
import os


def main(input_file_path, output_file_path, bib_id):

    f = None
    try:
        f = open(output_file_path, 'a+')
        html_tag = "<url><loc>" + "http://sif.library.illinois.edu/bibframe/html/" + bib_id + "</loc>"
        f.write(html_tag + '<lastmod>2015-07-18</lastmod><changefreq>daily</changefreq><priority>0.5</priority></url>')

    except Exception as e:
        print('Error converting file - ', input_file_path, str(e))
        if os.path.exists(output_file_path):
            os.remove(output_file_path)
    finally:
        if f is not None:
            f.close()

if __name__ == '__main__':
    try:
        #input_folder_path = sys.argv[1]
        #output_folder_path = sys.argv[2]

        input_folder_path = '/Users/jimhahn/Desktop/jimhahn/html29'
        output_folder_path = '/Users/jimhahn/Desktop/sitemap29'
    except IndexError:
        print('Please provide all input parameters\nUsage: python Instance.py /path/to/folder/containing/Master/RDF/files /path/to/folder/to/store/Instance/RDFs')
        sys.exit(0)


    for root, dirs, file_names in os.walk(input_folder_path):
        for file in file_names:
            input_file_path = root + '/' + file
            bib_id = file.split('_')[0]
            output_file_name = 'sitemap29.xml'
            output_file_path = output_folder_path + '/' + output_file_name
            main(input_file_path, output_file_path, bib_id)